from PIL import Image
import scipy.ndimage

letters = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c',
           'v', 'b', 'n', 'm', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
# 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X',\]
#  'C', 'V', 'B', 'N', 'M',


def load_images(folder, extension='.png'):
    """
    @param folder: folder where letter images are
    @param extension: extension of images of letters
    @returns: dictionary of letter -> image
    """
    images = dict()

    for l in letters:
        img = (255 - scipy.ndimage.imread(folder + '/' + l + extension, flatten=True))
        images[l] = img

    return images


def prepare_images(file_name, directory, width=15, height=28):
    """
    @param file_name: file in which there is a full font
    @directory: file to which you want to save files
    @returns: doesn't return anything, only saves to disk
    """
    font_image = Image.open(file_name)

    for i, l in enumerate(letters):
        img = font_image.crop((i * width, 3, (i + 1) * width, height))
        img.save(directory + '/' + l + '.png')

    print 'Images prepared! In directory: ' + directory