import Tkinter
from tkFileDialog import askopenfilename
from ocr import Recognizer
import tkMessageBox
from PIL import Image, ImageTk
import thread


class Window:

    def __init__(self, width=1366, height=768, filename='', threshold=0.992):
        self.width = width
        self.height = height
        self.top = Tkinter.Tk()
        self.top.wm_state('zoomed')
        self.filename = filename

        self.image_button = Tkinter.Button(self.top, text='Load Image', command=self.image_button_action)
        self.recognize_button = Tkinter.Button(self.top, text='Recognize!', command=self.recognize_button_action)
        self.image_label = Tkinter.Label(self.top)

        self.text_box = Tkinter.Text(self.top)
        self.file_box = Tkinter.Text(self.top)

        self.recognizer = Recognizer(threshold=threshold)

    def image_button_action(self):
        self.filename = askopenfilename()
        self.file_box.delete('1.0', Tkinter.END)
        self.file_box.insert('0.0', self.filename)

        photo = ImageTk.PhotoImage(Image.open(self.filename))

        self.image_label.config(image=photo)
        self.image_label.image = photo

    def recognize_button_action(self):
        try:
            text = self.recognizer.get_text(self.filename)
            tkMessageBox.showinfo('Recognizing', 'Done!')
            self.text_box.delete('1.0', Tkinter.END)
            self.text_box.insert('1.0', text)
        except IOError:
            tkMessageBox.showerror('File error', 'No such file. Choose another one')

    def run(self):
        self.image_button.pack()
        self.image_button.place(x=self.width - 10 - 150, y=10, width=150, height=80)

        self.recognize_button.pack()
        self.recognize_button.place(x=10, y=10, width=150, height=80)

        self.text_box.pack()
        self.text_box.place(x=20, y=100, width=self.width - 40, height=100)
        self.text_box.insert('1.0', 'Here will be placed recognized text.')

        self.file_box.pack()
        self.file_box.place(x=180, y=20, width=self.width-300-20-40, height=60)
        self.file_box.insert('1.0', 'Please choose your file.')

        self.image_label.pack()
        self.image_label.place(x=20, y=210, width=self.width - 40, height=self.height - 250)
        self.image_label.config(relief=Tkinter.SOLID)

        self.top.mainloop()