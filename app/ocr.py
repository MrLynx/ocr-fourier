from parse.files import prepare_images
from parse.files import load_images
from numpy.fft import fft2, ifft2
import scipy.ndimage
from numpy import rot90
from numpy import amax
import numpy as np


class Recognizer:

    def __init__(self, threshold):
        prepare_images('../resources/console.png', '../resources/fonts/console')
        self.threshold = threshold
        self.images_dict = load_images('../resources/fonts/console')
        self.image = None

    def get_letter_positions(self, letter):
        """
        @param letter: letter for which we get positions
        @returns: list of (X,Y, letter) where upper left corner of letters are
        """

        convolution = ifft2(fft2(self.image) * fft2(rot90(self.images_dict[letter], 2), s=self.image.shape)).real

        maximum = np.amax(convolution)

        x = np.where(convolution >= maximum * self.threshold)[0]
        y = np.where(convolution >= maximum * self.threshold)[1]

        return zip(x, y, convolution[x, y], [letter for _ in xrange(len(x))])

    def get_text(self, image_path):
        """
        @param image: image of text which we want to recognize
        @returns: text which is on image
        """
        self.image = scipy.ndimage.imread(image_path, flatten=True)
        self.image = (255 - self.image)

        all_positions = []

        for letter in self.images_dict.keys():
            all_positions.extend(self.get_letter_positions(letter))

        # all_positions.sort(key=lambda (x, y, c, l): c, reverse=False)
        # all_positions.sort(key=lambda (x, y, c, l): y)
        # all_positions.sort(key=lambda (x, y, c, l): x)
        #

        p = sorted(all_positions, key=lambda (x, y, c, l): (x/5, y/5, -c))

        text = ''

        dy, dx = p[0][1] - 15, p[0][0]
        for x, y, v, l in p:
            print(l, v, x/5, y/5)
            if abs(dy - y) > 25 and abs(dx - x) < 2:
                text += ' '

            if abs(dx - x) > 5:
                text += '\n'

            if abs(dy - y) > 10:
                text += l

            dx, dy = x, y



        return text